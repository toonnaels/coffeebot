DROP TABLE IF EXISTS `activities`;
DROP TABLE IF EXISTS `admins`;
DROP TABLE IF EXISTS `purchases`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `unregisteredUsers`;
DROP TABLE IF EXISTS `coffeeOptions`;

CREATE TABLE IF NOT EXISTS `activities` (`id` CHAR(36) BINARY , `date` DATETIME, `description` VARCHAR(255), PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `admins` (`id` CHAR(36) BINARY , `email` VARCHAR(255) UNIQUE, `password` VARCHAR(255) UNIQUE, `name` VARCHAR(54), `surname` VARCHAR(54), PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `users` (`id` CHAR(36) BINARY , `studentNumber` VARCHAR(8), `cardUUID` VARCHAR(12) UNIQUE, `name` VARCHAR(54), `surname` VARCHAR(54), `balance` DOUBLE PRECISION, PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `coffeeOptions` (`id` CHAR(36) BINARY , `item` VARCHAR(20) UNIQUE, `price` DOUBLE PRECISION, PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `purchases` (`id` CHAR(36) BINARY , `userId` CHAR(36) BINARY, `itemId` CHAR(36) BINARY, `date` DATETIME, PRIMARY KEY (`id`), FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE, FOREIGN KEY (`itemId`) REFERENCES `coffeeOptions` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `unregisteredUsers` (`id` CHAR(36) BINARY , `cardUUID` VARCHAR(12), PRIMARY KEY (`id`)) ENGINE=InnoDB;

INSERT INTO `admins` (`id`,`email`,`password`,`name`,`surname`) VALUES ('54a7fac6-5565-4b2c-9e34-20175420e15b','esl@fakemail.com','esl','esl','lab');
INSERT INTO `coffeeOptions` (`id`,`item`,`price`) VALUES ('00d154b6-3853-4be9-b915-7d5946269bad','coffee',2.5);
INSERT INTO `coffeeOptions` (`id`,`item`,`price`) VALUES ('1ebbce09-99cb-4f12-ad4e-9629335013ec','milk',1);
INSERT INTO `coffeeOptions` (`id`,`item`,`price`) VALUES ('46cd752e-c32b-4783-86b0-d90a971c4375','milkcoffee',3.5);


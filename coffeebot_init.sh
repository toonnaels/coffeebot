#!/bin/bash

# Update
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

mkdir ~/coffeebot-deps

# Basics
sudo apt-get install -y git vim
sudo apt-get install -y build-essential python-dev python-pip python3-dev python3-pip

# GPIO
sudo pip install RPi.GPIO
sudo pip3 install RPi.GPIO

# RFID
sudo apt-get install -y python-spidev python3-spidev

cd ~/coffeebot-deps
git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
sudo python setup.py install
sudo python3 setup.py install
cd ~

# OLED
sudo apt-get install -y python-imaging python3-pil python-smbus python3-smbus

cd ~/coffeebot-deps
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python setup.py install
sudo python3 setup.py install
cd ~

# Coffeebot
git clone https://toonnaels@bitbucket.org/toonnaels/coffeebot.git
cd coffeebot
git submodule init
git submodule update
cd ~

# Docker
curl -fsSL https://get.docker.com -o ~/coffeebot-deps/get-docker.sh
sudo sh ~/coffeebot-deps/get-docker.sh
sudo usermod -aG docker pi
sudo apt-get install -y docker-compose

# Startup
mkdir -p /home/pi/.config/lxsession/LXDE-pi/autostart
cp /home/pi/coffeebot/coffeebot_autostart /home/pi.config/lxsession/LXDE-pi/
mv /home/pi/coffeebot/coffeebot_autostart /home/pi/coffeebot/autostart 

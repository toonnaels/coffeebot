# ESL Coffeebot

The ESL coffeebot rapsberry pi setup and web-server to keep track of the student's coffee balances.

## Project Overview

The project consists of:

1.  coffeebot-hw: The python script running the OLED screen and RFID card scanner for reading student cards.
2.  coffeebot-mariadb: The database, running MariaDB.
3.  coffeebot-api: The web-server backend running NodeJS.
4.  coffeebot-ng: The web-server frontend running AngularJS.

The web-server components (mariadb, api and ng) are run in Docker containers on the raspberry pi, serving the website and database.
Each web-server component has 2 docker files: Dockerfile.dev and Dockerfile.prod.
dev: Building for a development machine, to code and test.
prod: Building for the production machine (raspberry pi).
Each web-server component contains 2 scripts: build-dev.sh and build-prod.sh to build it for development or production.
These scripts should be executed through build-all-dev.sh and build-all-prod.sh to deploy the web-server.

## Raspberry Pi

To setup the raspberry pi for production:

1.  Install the hardware: OLED screen, RFID scanner, LEDs, buttons and RTC (look here for RTC setup instructions: https://www.raspberrypi-spy.co.uk/2015/05/adding-a-ds3231-real-time-clock-to-the-raspberry-pi/).
2.  Install Raspbian desktop
3.  Turn on the Pi and setup (user, password, etc. Note the IP address of the Pi)
4.  Run sudo raspi-config and turn on ssh, spi and i2c.
5.  Run the coffeebot_init.sh script to install all dependencies.
6.  Reboot.
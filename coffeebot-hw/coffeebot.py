import time
import requests
import os

import RPi.GPIO as GPIO

import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import MFRC522

# Constants
PADDING = 1
FONT_SIZE_SMALL = 12
FONT_SIZE_NORMAL = 14

LEFT = 1
CENTER = 2
RIGHT = 3

TOP = 1
MIDDLE = 2
BOTTOM = 3

COFFEE_BTN = 33
MILK_BTN = 35
ESC_BTN = 37

BTN_WAIT = 0.25

LOGIN_TIMEOUT = 7
COFFEE_AND_MILK_WAIT = 0.1
OPTION_TIMEOUT = 2
NEW_USER_TIMEOUT = 120

RED_LED = 29
GREEN_LED = 31

BUZZER_PIN = 40

ANTON_UID = "25115160192"
CHRISTIAAN_UID = "2081020537"
NEW_UID = "20212290211"

MUSIC = True

GET_STUDENT = "http://localhost:3000/api/users/cardId/"
REGISTER_NEW_USER = "http://localhost:3000/api/unregisteredUsers/register/"
PURCHASE = "http://localhost:3000/api/users/purchase/"

DIR = "/home/pi/coffeebot/coffeebot-hw/"

# Global variables
# RFID
rfid = None

# OLED
disp = None
draw = None
image = None
width = None
height = None

font_small = None
font_normal = None

# Drawing
top = None
bottom = None
lastPos = None

# Buttons
coffee_pressed = False
milk_pressed = False
esc_pressed = False

coffee_time_pressed = None
milk_time_pressed = None
esc_time_pressed = None

# Buzzer
buzzer = None

# User Control
logged_in = False

def initRFID():
    global rfid

    # Initialize RFID
    rfid = MFRC522.MFRC522()
    return rfid

def initOLED():
    global disp
    global draw
    global image
    global font_small
    global font_normal
    global width
    global height
    global top
    global bottom

    # Initialize OLED and clear display
    disp = Adafruit_SSD1306.SSD1306_128_64(rst=None, gpio=GPIO)
    disp.begin()

    # Create blank image for drawing
    width = disp.width
    height = disp.height
    image = Image.new('1', (width, height))

    draw = ImageDraw.Draw(image)

    # Load font
    #font_small = ImageFont.load_default()
    font_small = ImageFont.truetype(DIR + 'FiraSans-Light.otf', FONT_SIZE_SMALL)
    font_normal = ImageFont.truetype(DIR + 'FiraSans-Bold.otf', FONT_SIZE_NORMAL)

    # Drawing constants
    top = PADDING
    bottom = height - PADDING

def initButtons():
    GPIO.setup(COFFEE_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(MILK_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(ESC_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def initLEDs():
    GPIO.setup(RED_LED, GPIO.OUT)
    GPIO.setup(GREEN_LED, GPIO.OUT)

def initBuzzer():
    global buzzer

    GPIO.setup(BUZZER_PIN, GPIO.OUT)
    buzzer = GPIO.PWM(BUZZER_PIN, 440)

def delay(ms):
    if ms <= 0.2:
        now = time.time()
        while time.time() - now <= ms/1000.0:
            pass
    else:
        time.sleep(ms/1000.0)

def tone(freq, durationMS):
    buzzer.ChangeFrequency(freq)
    delay(durationMS)

def loginSound():
    if MUSIC:
        buzzer.start(50)

        tone(988, 100)
        tone(1319, 200)

        buzzer.stop()

def negativeSound():
    if MUSIC:
        buzzer.start(50)

        tone(392, 35)
        tone(784, 35)
        tone(1568, 35)

        buzzer.stop()

def positiveSound():
    if MUSIC:
        buzzer.start(50)

        tone(1319, 125)
        delay(5)
        tone(1568, 125)
        delay(5)
        tone(2637, 125)
        delay(5)
        tone(2093, 125)
        delay(5)
        tone(2349, 125)
        delay(5)
        tone(3136, 125)

        buzzer.stop()

def pressedSound():
    if MUSIC:
        buzzer.start(50)

        tone(1000, 100)

        buzzer.stop()

def readRFID():
    (status, TagType) = rfid.MFRC522_Request(rfid.PICC_REQIDL)
    (status, uid) = rfid.MFRC522_Anticoll()

    if status == rfid.MI_OK:
        return uid
    
    return None

def clearDisplay():
    disp.clear()
    clearCanvas()
    display()

def clearCanvas():
    global lastPos

    draw.rectangle((0, 0, width, height), outline=0, fill=0)
    lastPos = None

def display():
    displayAsync()

def displayAsync():
    disp.image(image)
    disp.display()

# Draw given string at given position
# string - text to display
# align - left, center, right
# position - top, middle, bottom, None to continue from last text
def text(string, align = LEFT, position = None, fontSize = FONT_SIZE_NORMAL):
    global lastPos

    # Rest position if undefined
    if lastPos == None:
        lastPos = (PADDING, top)

    # Obtain text size
    font = font_normal
    if fontSize == FONT_SIZE_SMALL:
        font = font_small
    size = draw.textsize(string, font=font)
    size = size[0] # Get width

    # Adjust position to draw text
    if position == TOP:
        lastPos = (PADDING, top)
    elif position == MIDDLE:
        lastPos = (PADDING, height / 2 - fontSize/2)
    elif position == BOTTOM:
        lastPos = (PADDING, height - fontSize - PADDING)

    if align == CENTER:
        lastPos = ((width - size)/2, lastPos[1])
    elif align == RIGHT:
        lastPos = (width - size - PADDING, lastPos[1])

    # Draw text and update next text position
    draw.text(lastPos, string, font=font, fill=255)
    lastPos = (PADDING, lastPos[1] + fontSize)

# Leave a small gap after the last text
def gap():
    global lastPos

    lastPos = (PADDING, lastPos[1] + 6)

def purchase(student, option):
    res = requests.get(PURCHASE + str(student["id"]) + "/" + str(option))

    if res.status_code >= 400:
        return None
    else:
        return res.json()

def showChooseOptions(studentName, balance):
    clearCanvas()

    text('ESL Coffeebot', CENTER, TOP)
    gap()
    text('Welcome ' + studentName + '!', CENTER, fontSize = FONT_SIZE_SMALL)
    text('Please select an option.', CENTER, fontSize = FONT_SIZE_SMALL)
    text('Balance: R{:.2f}'.format(balance), RIGHT, BOTTOM, FONT_SIZE_SMALL)

    display()

def showNewUser(uidArr):
    cardUUID = str(uidArr[0]) + "-" + str(uidArr[1]) + "-" + str(uidArr[2]) + "-" + str(uidArr[3])

    clearCanvas()

    text('Unregistered User', CENTER, TOP)
    gap()
    text('Card UUID:', CENTER, fontSize = FONT_SIZE_SMALL)
    text(cardUUID, CENTER, fontSize = FONT_SIZE_SMALL)
    gap()
    text('Press ESC when done.', CENTER, fontSize = FONT_SIZE_SMALL)

    display()

def serverError():
    clearCanvas()

    text('Server Error', CENTER, TOP)
    text('Something went wrong.', CENTER, MIDDLE, fontSize = FONT_SIZE_SMALL)

    display()

def coffeeMilkPressed(student):
    updated = purchase(student, 'milkcoffee')
    if updated == None:
        serverError()
    else:
        clearCanvas()

        text('ESL Coffeebot', CENTER, TOP)
        gap()
        text("Option: Coffee and Milk", CENTER, fontSize = FONT_SIZE_SMALL)
        text('Balance: R{:.2f}'.format(updated["balance"]), RIGHT, BOTTOM, FONT_SIZE_SMALL)

        display()

def coffeePressed(student):
    updated = purchase(student, 'coffee')
    if updated == None:
        serverError()
    else:
        clearCanvas()

        text('ESL Coffeebot', CENTER, TOP)
        gap()
        text("Option: Coffee", CENTER, fontSize = FONT_SIZE_SMALL)
        text('Balance: R{:.2f}'.format(updated["balance"]), RIGHT, BOTTOM, FONT_SIZE_SMALL)

        display()

def milkPressed(student):
    updated = purchase(student, 'milk')
    if updated == None:
        serverError()
    else:
        clearCanvas()

        text('ESL Coffeebot', CENTER, TOP)
        gap()
        text("Option: Milk", CENTER, fontSize = FONT_SIZE_SMALL)
        text('Balance: R{:.2f}'.format(updated["balance"]), RIGHT, BOTTOM, FONT_SIZE_SMALL)

        display()

def getStudent(uidArr):
    cardUUID = str(uidArr[0]) + "" + str(uidArr[1]) + "" + str(uidArr[2]) + "" + str(uidArr[3])

    res = requests.get(GET_STUDENT + cardUUID)

    if res.status_code >= 400:
        return None
    else:
        return res.json()

def registerNewUser(uidArr):
    cardUUID = str(uidArr[0]) + "" + str(uidArr[1]) + "" + str(uidArr[2]) + "" + str(uidArr[3])

    res = requests.get(REGISTER_NEW_USER + cardUUID)

    if res.status_code >= 400:
        return False
    else:
        return True

def main():
    global logged_in
    global coffee_pressed
    global milk_pressed
    global esc_pressed

    # Initialize
    initRFID()
    initOLED()
    initButtons()
    initLEDs()
    initBuzzer()
    print("Press Ctrl-C to stop.")

    optionChosen = 0

    try:
        while True:

            try:
                # Clear display
                if time.time() - optionChosen >= OPTION_TIMEOUT:
                    clearDisplay()
                    GPIO.output(RED_LED, GPIO.LOW)
                    GPIO.output(GREEN_LED, GPIO.LOW)

                # Read card
                uid = readRFID()

                if uid:
                    os.system('xset dpms force on && xset s reset')
                    student = getStudent(uid)
                    if student:
                        logged_in = True
                        loginSound()
                        GPIO.output(GREEN_LED, GPIO.HIGH)
                        GPIO.output(RED_LED, GPIO.HIGH)

                        showChooseOptions(student["name"], student["balance"])

                        now = time.time()
                        while time.time() - now <= LOGIN_TIMEOUT:
                            # Coffee pressed
                            if GPIO.input(COFFEE_BTN) == GPIO.LOW:
                                # Wait for milk
                                innerNow = time.time()
                                while time.time() - innerNow <= COFFEE_AND_MILK_WAIT:
                                    if GPIO.input(MILK_BTN) == GPIO.LOW:
                                        milk_pressed = True
                                coffee_pressed = True
                                pressedSound()
                                break

                            # Milk pressed
                            if GPIO.input(MILK_BTN) == GPIO.LOW:
                                # Wait for coffee
                                innerNow = time.time()
                                while time.time() - innerNow <= COFFEE_AND_MILK_WAIT:
                                    if GPIO.input(COFFEE_BTN) == GPIO.LOW:
                                        coffee_pressed = True
                                milk_pressed = True
                                pressedSound()
                                break

                            # ESC pressed
                            if GPIO.input(ESC_BTN) == GPIO.LOW:
                                esc_pressed = True
                                pressedSound()
                                break

                        # Handle event
                        if coffee_pressed and milk_pressed:
                            coffeeMilkPressed(student)
                            optionChosen = time.time()
                        elif coffee_pressed:
                            coffeePressed(student)
                            optionChosen = time.time()
                        elif milk_pressed:
                            milkPressed(student)
                            optionChosen = time.time()
                        elif esc_pressed:
                            clearDisplay()
                    else:
                        if registerNewUser(uid):
                            showNewUser(uid)
                            GPIO.output(RED_LED, GPIO.HIGH)
                            negativeSound()

                            newUserNow = time.time()
                            while time.time() - newUserNow <= NEW_USER_TIMEOUT:
                                if GPIO.input(ESC_BTN) == GPIO.LOW:
                                    clearDisplay()
                                    break
                        else:
                            serverError()

                # Reset values
                logged_in = False

                coffee_pressed = False
                milk_pressed = False
                esc_pressed = False
            
            except requests.exceptions.RequestException:
                serverError()
                negativeSound()
                optionChosen = time.time()

    except KeyboardInterrupt:
        clearDisplay()
        GPIO.cleanup()

if __name__ == "__main__":
    main()

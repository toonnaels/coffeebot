#!/bin/bash

docker-compose down

cd ./coffeebot-mariadb
./build-dev.sh

cd ../coffeebot-api
./build-dev.sh

cd ../coffeebot-ng
./build-dev.sh

cd ..
cp .env-dev .env

cp ./docker-compose.dev.yml ./docker-compose.yml
docker-compose up --build -d

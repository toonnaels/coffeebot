#!/bin/bash

docker-compose down

cd ./coffeebot-mariadb
./build-prod.sh

cd ../coffeebot-api
./build-prod.sh

cd ../coffeebot-ng
./build-prod.sh

cd ..
cp .env-prod .env

cp ./docker-compose.prod.yml ./docker-compose.yml
docker-compose up --build -d
